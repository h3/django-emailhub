django-emailhub
===============

.. image:: https://gitlab.com/h3/django-emailhub/badges/master/build.svg
    :target: https://gitlab.com/h3/django-emailhub/pipelines

.. image:: https://readthedocs.org/projects/django-emailhub/badge/?version=latest
    :target: https://django-emailhub.readthedocs.io/en/latest/

.. image:: https://badge.fury.io/py/emailhub.svg
    :target: https://pypi.org/project/emailhub/


`Django EmailHub <http://django-emailhub.readthedocs.io/en/latest/>`_ is an
application that bring advanced email functionnalities to Django such as
templates, draft state, batch sending and archiving.


Supported Python/Django versions
--------------------------------

* django: **1.8, 1.9, 1.10, 1.11, 2.0, 2.1, 2.2, 3.0**
* python: **2.7, 3.4, 3.5, 3.6, 3.7**

**Note**: Work in progress, expect backward incompatible changes.


Links
-----

* `EmailHub Documentation <http://django-emailhub.readthedocs.io/en/latest/>`_
