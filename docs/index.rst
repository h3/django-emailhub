.. django-emailhub documentation master file, created by
   sphinx-quickstart on Mon Mar  5 18:32:45 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. figure:: topics/img/logo.png
    :alt: Django EmailHub Logo
    :align: center
    :width: 400px


Django EmailHub |version| documentation
=======================================

Django EmailHub is a application that bring advanced email
functionnalities to Django such as templates, batch sending
and archiving.

**Note**: This is a work in progress and in early development stage.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   topics/getting-started
   topics/settings
   topics/python-api
   topics/commands
   topics/templates
   topics/views
   topics/signals
   topics/integration


Overview
========


Inboxes
-------

Not actual inboxes, but emails are (optionally) linked to users model.

This makes it possible to build an inbox view for users where they
can see a copy of all emails sent to them.

This is accomplished in two ways, first when using the EmailHub API:


.. code:: python

    EmailFromTemplate('welcome-message').send_to(user)


And when using EmailHub's email backends, it will look for user
emails that matches the destination email and link them.


Batch sending
-------------

Sending email right away is rarely a good idea, having a batch sending
approach prevents many headaches down the road.

It won't hang your frontend process if the SMTP is slow to respond.

It allows to have throttling rules to avoid flooing the SMTP.

Finally, it allow to introduce the draft state feature.


Draft state
-----------

The draft mode works somewhat like standard email draft, but with automated
emails.

When a template email is sent and draft mode is enabled, the email isn't sent
right away. It is only saved in db where it can be edited and sent at a later
time.

This allows to create new email templates and review / correct outgoing emails
before they are actually sent to actual customers.

When the template is stable, draft mode can be disabled and be sent directly.


Email templates
---------------

Email templates are can be defined in the admin. They support:

- translations
- variables (they are actual django templates)
- preset signatures
- overriding default send from email
- allow or block draft mode
- django-material theme

They also integrate CodeMirror to highlight template variables and HTML:

.. figure:: topics/img/edit-emailtemplate.png
    :alt: Email template admin editing
    :align: center
    :width: 1003px

.. note:: This screenshot is with the Django-material theme.


Generic views
-------------

EmailHub provide generic views for ``EmailMessage`` for viewing, editing and
listing. You can consult the :doc:`Generic Views documentation here <topics/views>`.


Signature templates
-------------------

Email templates can (or not) use signature templates defined in the admin.


Ecosystem
---------

Django EmailHub tries to stay compatible with the followings apps:

* `django-material <http://forms.viewflow.io/>`_
* `django-anymail <https://github.com/anymail/django-anymail>`_ (refer to backend settings for integration)
* `django-herald <https://github.com/worthwhile/django-herald>`_
* `django-secure-mail <https://github.com/blag/django-secure-mail>`_

Don't hesitate to create a pull request to integrate you django app, I will
gladly merge it!

You might also want to consider `django-post-office <https://github.com/ui/django-post_office>`_
which has overlapping features with EmailHub.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
