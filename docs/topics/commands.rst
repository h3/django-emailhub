Management commands
===================


Create template
---------------

.. code-block:: bash

    (venv)$ python manage.py emailhub --create-template


Diff
----

Performs a diff between a JSON fixture file and template present in database.

.. code-block:: bash

    (venv)$ python manage.py emailhub --diff emailtemplates-backup.json

By default only changed field are shown, to get a full word level diff you
can set verbosity at 2.


.. code-block:: bash

    (venv)$ python manage.py emailhub --diff emailtemplates-backup.json -v2


Dump
----

Dumps specific email templates specified by slug in JSON format.

Multiple slug can be passed using comas as separators.

.. code-block:: bash

    (venv)$ python manage.py emailhub --dump request-accepted,request-received


Dump all
--------

Dumps all email template in JSON format.


List templates
--------------

List available templates and their corresponding translations.

.. code-block:: bash

    (venv)$ python manage.py emailhub --list-templates

    request-accepted
      - EN) [{{ site }}] We have accepted your request!
      - FR) [{{ site }}] Nous aons accepté votre demande!

    request-received
      - EN) [{{ site }}] New request from {{ user.get_fullname }}
      - FR) [{{ site }}] Nouvelle demande de {{ user.get_fullname }}


Send
----

Send unsent emails:

.. code-block:: bash

    (venv)$ python manage.py emailhub --send


Since email batch sending is throttled, you can set up a cron job to run every
minutes to send unsent emails. This way an email will never wait more than one
minute before being sent in a optimal situation.

.. code-block:: bash

    *  * * * * root /venv/path/bin/python /project/path/manage.py emailhub --send >> /var/log/emailhub.log 2>&1



Send test
---------

Sends an email test, accepts a destination email address or a user ID.

.. code-block:: bash

    (venv)$ python manage.py emailhub --send-test bob@test.com
    Content-Type: text/plain; charset="utf-8"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    Subject: Test email
    From: no-reply@domain.com
    To: bob@test.com
    Date: Tue, 06 Mar 2018 19:01:16 -0000
    Message-ID: <20180306190116.2915.53062@singularity>

    This is a test.
    -------------------------------------------------------------------------------


Status
------

.. code-block:: bash

    (venv)$ python manage.py emailhub --status

	Unsent                        14
	Drafts                        7
	Is sent                       7
	Errors                        0
