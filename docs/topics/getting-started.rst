Getting started
===============


Installation
------------

The project is not stable enough yet to be on Pypy, to use it you will need to
use the git repository:

.. code:: bash

    pip install git+https://gitlab.com/h3/django-emailhub.git


Add `emailhub` to your project's settings:

.. code:: python

    INSTALLED_APPS = [
        ...
        'emailhub',
    ]

Run migrations:

.. code:: bash

    (venv)$ python manage.py migrate


Configuration
-------------

In order to be able to log all outgoing emails, not just those sent from
templates, it is necessary to use EmailHub's email backends:

.. code:: python

    EMAIL_BACKEND = 'emailhub.backends.smtp.EmailBackend'
    # or
    EMAIL_BACKEND = 'emailhub.backends.console.EmailBackend'

Refer to the :doc:`settings documentation <settings>` for all available backends.
