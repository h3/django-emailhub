Integration
===========

Django REST Regristration
-------------------------

The `django-rest-registration <https://github.com/apragacz/django-rest-registration>`__
is not really flexible in term of template engine for emails.

There are however workarounds.

Registration view
~~~~~~~~~~~~~~~~~

The registration view can use emailhub template if you disable
`REGISTER_VERIFICATION_ENABLED` and use signal to send the notification instead:

.. code:: python

    # settings.py
    REST_REGISTRATION = {
      'REGISTER_VERIFICATION_ENABLED': False,
    }


.. code:: python


    # signals.py
    from django.conf import settings
    from django.dispatch import receiver

    from rest_registration.api.views.register import RegisterSigner
    from rest_registration.utils.users import get_user_verification_id
    from emailhub.utils.email import EmailFromTemplate

    @receiver(user_registered, sender=None)
    def user_registred(sender, **kwargs):
        user, request = kwargs.get('user'), kwargs.get('request')
        with transaction.atomic():
            signer = RegisterSigner({
                'user_id': get_user_verification_id(user),
            }, request=request)
            context = {
                'site': settings.FRONTEND_URL.split('//').pop(-1),
                'site_url': settings.FRONTEND_URL,
                'params_signer': signer,
                'verification_url': settings.REST_REGISTRATION.get(
                    'REGISTER_VERIFICATION_URL'),
            }
            EmailFromTemplate(
                'register-verify', extra_context=context,
                lang=user.language).send_to(user)



.. code:: python

    # views.py
    from django.conf import settings

    from rest_registration.decorators import api_view_serializer_class_getter
    from rest_registration.settings import registration_settings
    from rest_registration.notifications.enums import NotificationType
    from rest_framework.decorators import api_view, permission_classes
    from rest_framework.permissions import AllowAny
    from rest_registration.utils.responses import get_ok_response
    from rest_registration.utils.users import get_user_verification_id
    from rest_registration.exceptions import UserNotFound
    from rest_registration.api.views.reset_password import ResetPasswordSigner
    from emailhub.utils.email import EmailFromTemplate


    @api_view_serializer_class_getter(
        lambda: registration_settings.SEND_RESET_PASSWORD_LINK_SERIALIZER_CLASS)
    @api_view(['POST'])
    @permission_classes([AllowAny])
    def send_reset_password_link(request):
        '''
        Send email with reset password link.
        '''
        if not registration_settings.RESET_PASSWORD_VERIFICATION_ENABLED:
            raise Http404()
        serializer_class = registration_settings.SEND_RESET_PASSWORD_LINK_SERIALIZER_CLASS  # noqa: E501
        serializer = serializer_class(
            data=request.data,
            context={'request': request},
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.get_user_or_none()
        if not user:
            raise UserNotFound()
        signer = ResetPasswordSigner({
            'user_id': get_user_verification_id(user),
        }, request=request)

        EmailFromTemplate(
            'password-reset', extra_context={
                'site': settings.FRONTEND_URL.split('//').pop(-1),
                'site_url': settings.FRONTEND_URL,
                'params_signer': signer,
                'verification_url': signer.get_url(),
                'user': user,
            }, lang=user.language).send_to(user)

        return get_ok_response('Reset link sent')
