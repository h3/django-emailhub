Settings
========


General configurations
----------------------

**EMAIL_BACKEND**

In order to be able to log all outgoing emails, not just those sent from
templates, it is necessary to use EmailHub's email backends.


.. code:: python

    EMAIL_BACKEND = 'emailhub.backends.smtp.EmailBackend'

They are essentially subclasses of the core django email backends.

Here's the conversion table for generic Django backends:

+------------------------------------------------------+----------------------------------------------+
| Django                                               | EmailHub                                     |
+======================================================+==============================================+
| ``django.core.mail.backends.smtp.EmailBackend``      | ``emailhub.backends.smtp.EmailBackend``      |
+------------------------------------------------------+----------------------------------------------+
| ``django.core.mail.backends.console.EmailBackend``   | ``emailhub.backends.console.EmailBackend``   |
+------------------------------------------------------+----------------------------------------------+
| ``django.core.mail.backends.filebased.EmailBackend`` | ``emailhub.backends.filebased.EmailBackend`` |
+------------------------------------------------------+----------------------------------------------+
| ``django.core.mail.backends.locmem.EmailBackend``    | ``emailhub.backends.locmem.EmailBackend``    |
+------------------------------------------------------+----------------------------------------------+
| ``django.core.mail.backends.dummy.EmailBackend``     | ``emailhub.backends.dummy.EmailBackend``     |
+------------------------------------------------------+----------------------------------------------+

Django-Anymail
~~~~~~~~~~~~~~

Backends for `django-anymail <https://github.com/anymail/django-anymail>`_:

+-----------------------------------------------+--------------------------------------------------------+
|  AnyMail                                      | EmailHub                                               |
+===============================================+========================================================+
| ``anymail.backends.console.EmailBackend``     | ``emailhub.backends.anymail.console.EmailBackend``     |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.mailgun.EmailBackend``     | ``emailhub.backends.anymail.mailgun.EmailBackend``     |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.mailjet.EmailBackend``     | ``emailhub.backends.anymail.mailjet.EmailBackend``     |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.mandrill.EmailBackend``    | ``emailhub.backends.anymail.mandrill.EmailBackend``    |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.postmark.EmailBackend``    | ``emailhub.backends.anymail.postmark.EmailBackend``    |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.sendgrid.EmailBackend``    | ``emailhub.backends.anymail.sendgrid.EmailBackend``    |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.sendgrid_v2.EmailBackend`` | ``emailhub.backends.anymail.sendgrid_v2.EmailBackend`` |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.sendinblue.EmailBackend``  | ``emailhub.backends.anymail.sendinblue.EmailBackend``  |
+-----------------------------------------------+--------------------------------------------------------+
| ``anymail.backends.sparkpost.EmailBackend``   | ``emailhub.backends.anymail.sparkpost.EmailBackend``   |
+-----------------------------------------------+--------------------------------------------------------+

Django-Secure-Mail
~~~~~~~~~~~~~~~~~~

Backends for `django-secure-mail <https://github.com/blag/django-secure-mail>`_:

+----------------------------------------------------------+----------------------------------------------------------+
| Secure Mail                                              | EmailHub                                                 |
+==========================================================+==========================================================+
| ``secure_mail.backends.EncryptingSmtpEmailBackend``      | ``emailhub.backends.secure_mail.smtp.EmailBackend``      |
+----------------------------------------------------------+----------------------------------------------------------+
| ``secure_mail.backends.EncryptingConsoleEmailBackend``   | ``emailhub.backends.secure_mail.console.EmailBackend``   |
+----------------------------------------------------------+----------------------------------------------------------+
| ``secure_mail.backends.EncryptingFilebasedEmailBackend`` | ``emailhub.backends.secure_mail.filebased.EmailBackend`` |
+----------------------------------------------------------+----------------------------------------------------------+
| ``secure_mail.backends.EncryptingLocmemEmailBackend``    | ``emailhub.backends.secure_mail.locmem.Emailbackend``    |
+----------------------------------------------------------+----------------------------------------------------------+

Django-Celery-Email
~~~~~~~~~~~~~~~~~~~

Backends for `django-celery-email <https://github.com/pmclanahan/django-celery-email>`_:

+------------------------------------------------+----------------------------------------------------------+
| Django Celery Email                            | EmailHub                                                 |
+================================================+==========================================================+
| ``djcelery_email.backends.CeleryEmailBackend`` | ``emailhub.backends.djcelery_email.celery.EmailBackend`` |
+------------------------------------------------+----------------------------------------------------------+


**EMAILHUB_DRAFT_MODE**

Defaul: ``True``

Activate or deactivate draft mode.


**EMAILHUB_SEND_HTML**

Default: ``True``

Send also the HTML version with the text version of the email body (multi-parts)


**EMAILHUB_PAGINATE_BY**

Default: ``20``

Pagination count used by EmailMessage ListView.


**EMAILHUB_USER_LANGUAGE_DETECTION**

Default: ``True``

If set to ``True``, templates rendering will be rendered with the user's language
if it can be resoved. See `EMAILHUB_USER_LANGUAGE_RESOLVER` to see how
language is resolved or customize it.

If set to ``Fales``, the ``settings.LANGUAGE_CODE`` will be used to render email
templats if no language is provided.


**EMAILHUB_USER_LANGUAGE_RESOLVER**

Default: ``'emailhub.utils.i18n.guess_user_language'``

This is a function used to guess a user's preferred language according to
common models patterns, **you should provide your own function to resolve
the language**.

This is the default resolver:

.. code:: python

    def guess_user_language(user):
        if hasattr(user, 'profile') and hasattr(user.profile, 'language'):
            return user.profile.language
        elif hasattr(user, 'profile') and hasattr(user.profile, 'lang'):
            return user.profile.lang
        elif hasattr(user, 'language'):
            return user.profile.language
        elif hasattr(user, 'lang'):
            return user.lang
        elif hasattr(settings, 'LANGUAGE_CODE'):
            return settings.LANGUAGE_CODE.split('-')[0]
        else:
            return 'en'

This what your custom resolver should look like:

.. code:: python

    def my_custom_resolver(user):
        return user.customerprofile.lang


Outgoing emails
---------------


**EMAILHUB_DEFAULT_FROM**

Default: ``'no-reply@domain.com'``


If email_from isn't specified when sending the email or if the template
does not provide a value for it, this setting is used.


**EMAILHUB_SEND_BATCH_SLEEP**

Default: ``2``


Sleep N seconds between sending each batches


**EMAILHUB_SEND_BATCH_SIZE**

Default: ``20``

Limit the number of Email objects will be sent


**EMAILHUB_SEND_MAX_RETRIES**

Default: ``3``

Maximum send retries before giving up.


Email templates
---------------


**EMAILHUB_PRELOADED_TEMPLATE_TAGS**

Default:

::

    [
        'i18n',
    ]

These template tags will be preloaded for email templates rendering.

**EMAILHUB_TEXT_TEMPLATE**

Default: ``"""{{% load {template_tags} %}}{content}"""``

Template used to render text email templates

**EMAILHUB_HTML_TEMPLATE**

Default:

.. code:: html

    {{% load {template_tags} %}}
    {{% language lang|default:"en" %}}
    <!DOCTYPE html>
    <html lang="{{ lang }}">
    <head><meta charset="utf-8"></head>
    <body>{content}</body>
    </html>

Template used to render HTML email templates
