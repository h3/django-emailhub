Signals
=======


on_email_process
----------------

Sent when an ``EmailMessage`` is created from an outgoing email.

.. code:: python

    from emailhub.signals import on_email_process

    on_email_process.connect(email_process_callback,
                            dispatch_uid="emailhub.on_email_process")


on_email_out
------------

Sent when an email is going out.

.. code:: python

    from emailhub.signals import on_email_out

    on_email_out.connect(email_out_callback, dispatch_uid="emailhub.on_email_out")
