Templates
=========

Email templates use Django's built-in template renderer, which means you can
load and use any available templatetags.

Base context
------------

The base context contains global variables.

