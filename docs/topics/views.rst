Generic Views
=============

Generic views can be used as is or be subclassed for customization.

**Example**:

.. code:: python

    from emailhub.views import InboxListView


    class MessageIndexView(InboxListView):
        """
        EmailMessage inbox view
        """
        pass


InboxListView
-------------

The ``InboxListView`` view is used to create an Inbox view for the email
recipient (must be a registred used).


EmailMessageDetailView
----------------------

The ``EmailMessageDetailView`` view is used to display a specific message.

The message state must be in ``['locked', 'sent', 'error']``.


EmailMessageUpdateView
----------------------

The ``EmailMessageUpdateView`` view is used to edit a specific message.

The message state must be ``draft``.


process_message
---------------

Used manipulate message via ajax calls.

**Actions**:

* send
* delete
