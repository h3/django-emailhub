# -*- coding: utf-8 -*-
"""
EmailHub AnyMail SparkPost backend
"""
from anymail.backends.sparkpost import EmailBackend as BaseEmailBackend  # noqa pylint: disable=import-error
from emailhub.utils.email import process_outgoing_email


class EmailBackend(BaseEmailBackend):
    """ AnyMail SparkPost backend """
    def _send(self, message):  # noqa pylint: disable=missing-docstring
        process_outgoing_email(message)
        super(EmailBackend, self)._send(message)
